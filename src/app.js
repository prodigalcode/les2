require("./css/index.css");
require("./html/index.html");

var tester = require('./js/tester');

var data = [];

var margin = {
    top: 20,
    left: 40,
    right: 40,
    bottom: 40
};
var w = 800;
var h = 400;

function time(d) {
    return d.time
}
function xValueByName(prop) {
    return function (k) {
        return k ? x(k.key) : 0
    }
}

function yValueByName(prop) {
    return function (k) {
        return y(time(k[prop]))
    }
}

var y = d3.scale.linear()
    .range([h, 0])
    ;
var x = d3.time.scale()
    .range([0, w])
    ;

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom")
    .ticks(6)
    ;

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    ;

var svg = d3.select("body")
    .append("svg")
    .attr("width", w + margin.left + margin.right)
    .attr("height", h + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + [margin.left, margin.top] + ")")
    ;

var gXAxis = svg.append('g')
    .attr("class", "x-axis")
    .attr("transform", "translate(0, " + h + ")")
    .call(xAxis)
    ;

var gYAxis = svg.append('g')
    .attr("class", "y-axis")
    .call(yAxis)
    ;

svg.selectAll(".metrics")
    .data(["for", "while"])
    .enter()
    .append("g")
    .attr("class", "metrics")
;

function xy(k) {
    return [xValue(k), yValue(k)]
}
function add(arr, num) {
    return arr.map(function (a) {
        return a + num
    })
}

var text = svg
    .append("g")
    .style("display", "none")
    ;
text.append("rect")
    .attr({
        width: 32,
        height: 32,
        transform: "translate(-16, -16)"
    })
    .style({
        fill: "darkblue"
    });
text.append("text")
    .attr("class", "tip")
    .attr("dy", ".35em")
    ;

/*
var circles = svg.append("g");
circles.selectAll("circle")
    .data(data, function (d) {
        return d.date
    })
    .enter()
    .append("g")
    .attr("transform", function (k) {
        return "translate(" + xy(k) + ")"
    })
    .append("circle")
    .attr("r", 5)
    .on("mouseover", function (k) {
        text.style("display", null)
            .attr("transform", "translate(" + add(xy(k), 24) + ")")
            .select("text")
            .text(k.value)
    })
    .on("mousemove", function () {

    })
    .on("mouseout", function () {
        text.style("display", "none")
    })
;
*/

function update(data) {
    var ext1 = [Infinity, -Infinity]
        , ext2 = ext1.slice(0)
        ;

    data.forEach(function (d) {
        ext1[0] = Math.min(ext1[0], d.for.time, d.while.time);
        ext1[1] = Math.max(ext1[1], d.for.time, d.while.time);

        ext2[0] = Math.min(ext2[0], d.key);
        ext2[1] = Math.max(ext1[0], d.key);
    });

    y.domain(ext1);
    x.domain(ext2);

    gYAxis.call(yAxis);
    gXAxis.call(xAxis);
    svg.selectAll(".metrics")
        .each(function (d) {
            this.path = this.path || (
                d3.svg.line()
                    .x(xValueByName(d))
                    .y(yValueByName(d))
            );
            this.svg = this.svg || (
                svg.append("path")
                    .style("stroke",
                        "for" == d
                        ? "red"
                        : "blue"
                    )
            );

            this.svg.attr("d", this.path(data))
        });
}

var count = 1e6;
function startTests() {
    setTimeout(function () {
        tester.loops(count, function (item) {
            data.push(item);
            // valid = false;
            //  count *= 10;
            // if (!(count % 1e10))
            //     return;
            update(data);
            startTests();
        });
    }, 100)
}
startTests();

/*
function startPushTests() {
    setTimeout(function () {
        tester.arrs(count, function (item) {
            data.push(item);
            update(data);
            startPushTests();
        });
    }, 100)
}

d3.select("body")
    .selectAll("button")
    .data([
        {fn: startTests, name: "Loops"},
        {fn: startPushTests, name: "Push"}
    ])
    .enter()
    .append("button")
    .on("click", function (d) {
        d.fn && d.fn();
        d3.selectAll("button")
            .style("display", "none");
    })
    .text(function (d) {
        return d.name;
    })
;
*/

