var arr = [], arr2 = [];
module.exports = (function () {

    function testLoop(count, cb) {
        var result = {
            count: count,
            key: Date.now(),
            for: {
                name: "for",
                time: 0
            },
            while: {
                name: "while",
                time: 0
            }
        };


        var i = 0, l = count;

        var waiter = 2;

        function finish() {
            waiter--;
            if (!waiter)
                cb(result)
        }

        (function () {
            var temp = [];
            console.time("for");
            var start = Date.now();
            for(;i < count; i++) {
                temp.push(i);
            }
            var end = Date.now();
            console.timeEnd("for");
            result.for.time = end - start;
            finish()
        })();

        (function () {
            var temp = [];
            console.time("while");
            var start = Date.now();
            while(l--) {
                temp.push(l);
            }
            var end = Date.now();
            console.timeEnd("while");
            result.while.time = end - start;
            finish()
        })();
    }

    function testConcat(count, cb) {
        if (!arr.length)
            while (count--) {
                arr.push(Math.random());
            }
        if (!arr2.length)
            arr2 = arr.slice(0);
        var newArr = [], l = 1e5;
        while(l--) {
            newArr.push(l);
        }

        var result = {
            count: count,
            key: Date.now(),
            concat: {
                name: "concat",
                time: 0,
                memory: 0
            },
            push: {
                name: "push",
                time: 0,
                memory: 0
            }
        };

        var start = Date.now();
        var memory = performance.memory.usedJSHeapSize;
        arr = arr.concat(newArr);
        result.concat.time = Date.now() - start;
        result.concat.memory = performance.memory.usedJSHeapSize - memory;

        start = Date.now();
        memory = performance.memory.usedJSHeapSize;
        arr2.push.apply(arr2, newArr);
        result.push.time = Date.now() - start;
        result.push.memory = performance.memory.usedJSHeapSize - memory;
        // arr.length = 0;
        // arr2.length = 0;
        cb(result);
    }


    return {
        loops: testLoop
        , arrs: testConcat
    }
})();